package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 商品定義ファイル読み込み処理
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";

	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";

	private static final String UNKNOWN_FORMAT = "フォーマットが不正です";
	private static final String NOT_SERIAL_NUMBER = "売上ファイル名が連番になっていません。";
	private static final String UNKNOWN_SHOP_NUMBER = "の支店コードが不正です。";
	private static final String UNKNOWN_COMMODITY_NUMBER = "の商品コードが不正です。";
	private static final String NUMBER_TEN_OVER ="合計金額が10桁を超えました。";
	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		//errorm
		if(args.length !=1) {
			System.out.println(UNKNOWN_ERROR);
		}
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();

		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();

		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();
		String branchRegex = "^[0-9]{3}$";
		String commodityRegex = "^[0-9a-zA-Z]{8}$";
		// 支店定義ファイル読み込み処理 // 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, branchRegex, "支店")) {
			return;
		}
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, commodityRegex, "商品")) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		//該当ディレクトリのすべてのデータ
		for(int i = 0; i < files.length; i++) {
			String fileName = files[i].getName();
			// 売上ファイルの条件に当てはまったものだけをListに追加する。
			if(files[i].isFile() && fileName.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		//errorm 2-1 連番チェック
		for(int i = 0; i < rcdFiles.size()-1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));

			if((latter - former) != 1) {
				System.out.println(NOT_SERIAL_NUMBER);
				return;
			}
		}

		BufferedReader br = null;


		for(int i = 0; i < rcdFiles.size(); i++) {
			try {
				ArrayList<String> saleslist = new ArrayList<String>();

				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				String line;
				// 一行ずつ読み込む
				while((line = br.readLine()) != null) {

					// ※ここの読み込み処理を変更してください。(処理内容1-2)
					saleslist.add(line);
				}

				//errorm
				if(saleslist.size() != 3 ) {
					System.out.println(saleslist.get(i) + UNKNOWN_FORMAT);
					return;
				}

				//errorm
				if(! saleslist.get(2).matches("^[0-9]*$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}
				//errorm
				if(!branchNames.containsKey(saleslist.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + UNKNOWN_SHOP_NUMBER);
					return;
				}
//				errorm
				if(!commodityNames.containsKey(saleslist.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + UNKNOWN_COMMODITY_NUMBER);
					return;
				}

				//取り込んだ売上高をStringからlongへ型変換する
				long branchSale = Long.parseLong(saleslist.get(2));


				//取り出して＋足す
				Long saleAmount = branchSales.get(saleslist.get(0)) + branchSale;

				//商品別の売上を取り出して＋足す
				Long commoditySaleAmount = commoditySales.get(saleslist.get(1)) + branchSale;

				//errorm
				if(saleAmount >= 10000000000L ) {
					System.out.println(NUMBER_TEN_OVER);
					return;
				}

				if(commoditySaleAmount >= 1000000000L) {
					System.out.println(NUMBER_TEN_OVER);
					return;
				}

				//しまう
				branchSales.put(saleslist.get(0), saleAmount);
				commoditySales.put(saleslist.get(1), commoditySaleAmount);

			} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return;

			} finally {
					// ファイルを開いている場合
					if(br != null) {
						try {
							// ファイルを閉じる
							br.close();
						} catch(IOException e) {
							System.out.println(UNKNOWN_ERROR);
							return;
						}
					}
			}
		}
		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

		/**
		 * 支店定義ファイル読み込み処理
		 *
		 * @param フォルダパス
		 * @param ファイル名
		 * @param 支店コードと支店名を保持するMap
		 * @param 支店コードと売上金額を保持するMap
		 * @return 読み込み可否
		 */
		private static boolean readFile(String path, String fileName, Map<String, String> namesMap, Map<String, Long> salesMap,String regex, String fileType) {
			BufferedReader br = null;

			try {
				File file = new File(path, fileName);
				//errorm
				if(!file.exists()) {
					System.out.println(FILE_NOT_EXIST);
					return false;
				}

				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);

				String line;
				// 一行ずつ読み込む
				while((line = br.readLine()) != null) {
					String [] items = line.split(",");
					//errorm
					if((items.length != 2) || (!items[0].matches(regex))) {
						System.out.println(fileType + FILE_INVALID_FORMAT);
						return false;
					}

					// ※ここの読み込み処理を変更してください。(処理内容1-2)
					namesMap.put(items[0], items[1]);
					salesMap.put(items[0], 0L);
				}

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return false;
			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return false;
					}
				}
			}
			return true;
		}

		/**
		 * 支店別集計ファイル書き込み処理
		 *
		 * @param フォルダパス
		 * @param ファイル名
		 * @param 支店コードと支店名を保持するMap
		 * @param 支店コードと売上金額を保持するMap
		 * @return 書き込み可否
		 */
		private static boolean writeFile(String path, String fileName, Map<String, String> namesMap, Map<String, Long> salesMap) {
			// ※ここに書き込み処理を作成してください。(処理内容3-1)
			BufferedWriter bw = null;

			try {
				File file = new File(path, fileName);
				FileWriter fw = new FileWriter(file);
				bw = new BufferedWriter(fw);

				for(String key : salesMap.keySet()) {
					bw.write(key+ ","+ namesMap.get(key) + "," +  salesMap.get(key));
					bw.newLine();
				}
			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return false;
			}
			finally {
				if(bw != null) {
					try {
						bw.close();
					} catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return false;
					}
				}
			}
			return true;
		}
}
